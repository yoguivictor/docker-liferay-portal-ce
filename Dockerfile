# liferay/portal-ce:tomcat-6.2-ga4
FROM liferay/tomcat:7.0.42
RUN yum -y install wget unzip && yum clean all \
;   wget http://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.35.zip \
;   wget http://downloads.sourceforge.net/project/lportal/Liferay%20Portal/6.2.3%20GA4/liferay-portal-tomcat-6.2-ce-ga4-20150416163831865.zip \
;   unzip mysql-connector-java-5.1.35.zip \
;   unzip liferay-portal-tomcat-6.2-ce-ga4-20150416163831865.zip \
;   mv liferay-portal-6.2-ce-ga4 ${LIFERAY_HOME} \
;   mv mysql-connector-java-5.1.35/mysql-connector-java-5.1.35-bin.jar ${TOMCAT_HOME}/lib/ext/ \
;   rm -rf mysql-connector-java-5.1.35 *.zip

# Xuggler support
RUN yum -y install ImageMagick && yum clean all 
RUN wget http://xuggle.googlecode.com/svn/trunk/repo/share/java/xuggle/xuggle-xuggler/5.4/xuggle-xuggler-arch-x86_64-pc-linux-gnu.jar \
&& mv xuggle-* ${TOMCAT_HOME}/lib/ext

# GS support (PDFs)
RUN yum -y install ghostscript && yum clean all

